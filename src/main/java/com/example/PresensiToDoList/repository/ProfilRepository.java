package com.example.PresensiToDoList.repository;

import com.example.PresensiToDoList.modal.Presensi;
import com.example.PresensiToDoList.modal.Profil;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface ProfilRepository extends JpaRepository <Profil,Long> {

    @Query(value = "SELECT * FROM profil  WHERE users_id = ?1",nativeQuery = true)
    List<Profil> finduser(Long userId);




}
