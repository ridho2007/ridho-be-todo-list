package com.example.PresensiToDoList.repository;

import com.example.PresensiToDoList.modal.Presensi;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PresensiRepository extends JpaRepository <Presensi,Long> {
    @Query(value = "SELECT * FROM presensi  WHERE users_id = ?1",nativeQuery = true)
    List<Presensi> findAllUser(Long userId);
}
