package com.example.PresensiToDoList.repository;

import com.example.PresensiToDoList.modal.Presensi;
import com.example.PresensiToDoList.modal.TodoList;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface TodoListRepository extends JpaRepository <TodoList ,Long> {
    @Query(value = "SELECT * FROM todolist  WHERE users_id = ?1",nativeQuery = true)
    List<TodoList> findAllUser(Long userId);
}
