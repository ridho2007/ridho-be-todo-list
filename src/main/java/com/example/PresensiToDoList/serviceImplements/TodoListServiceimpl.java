package com.example.PresensiToDoList.serviceImplements;

import com.example.PresensiToDoList.dto.TodoListDto;
import com.example.PresensiToDoList.exception.NotFoundException;
import com.example.PresensiToDoList.modal.Presensi;
import com.example.PresensiToDoList.modal.TodoList;
import com.example.PresensiToDoList.repository.TodoListRepository;
import com.example.PresensiToDoList.repository.UsersRepository;
import com.example.PresensiToDoList.service.TodoListService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TodoListServiceimpl implements TodoListService {
    @Autowired
    TodoListRepository todoListRepository;

    @Autowired
    UsersRepository usersRepository;

    @Override
    public Object getAllTodoList(Long usersId) {
        return todoListRepository.findAllUser(usersId);
    }

    @Override
    public void deleteTodoList(Long id) {
        todoListRepository.deleteById(id);
    }

    @Override
    public TodoList getTodoList(Long id) {
        var todolist1 = todoListRepository.findById(id).get();
        return todoListRepository.save(todolist1);
    }

    @Override
    public TodoList addTodoList(TodoListDto todoListDto) {
        TodoList todoList1 = new TodoList();
        todoList1.setTask(todoListDto.getTask());
        todoList1.setJam(todoListDto.getJam());


        todoList1.setUsersId(usersRepository.findById(todoListDto.getUserId()).orElseThrow(() -> new NotFoundException("Not Found")));
        return todoListRepository.save(todoList1);
    }

    @Override
    public TodoList editTodoList(Long id, TodoList todoList) {
        TodoList todoList1 = todoListRepository.findById(id).get();

        todoList1.setTask(todoList.getTask());
        todoList1.setJam(todoList.getJam());


        return todoListRepository.save(todoList1);
    }

}
