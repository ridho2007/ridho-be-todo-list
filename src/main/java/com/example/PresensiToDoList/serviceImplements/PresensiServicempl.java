package com.example.PresensiToDoList.serviceImplements;

import com.example.PresensiToDoList.dto.MasukDto;
import com.example.PresensiToDoList.dto.PresensiDto;
import com.example.PresensiToDoList.enumated.PresensiEnum;
import com.example.PresensiToDoList.exception.NotFoundException;
import com.example.PresensiToDoList.modal.Presensi;
import com.example.PresensiToDoList.repository.PresensiRepository;
import com.example.PresensiToDoList.repository.UsersRepository;
import com.example.PresensiToDoList.service.PresensiService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class PresensiServicempl implements PresensiService {

    private static final Integer hour = 3600 * 1000;
    @Autowired
    PresensiRepository presensiRepository;

    @Autowired
    UsersRepository usersRepository;
    @Override
    public List<Presensi> getAllPresensi(Long usersId) {

        return presensiRepository.findAllUser(usersId);

    }

    @Override
    public Presensi absenMasuk(MasukDto masukDto, boolean type) {
        Presensi presensi1 = new Presensi();
        presensi1.setMasuk(new Date(new Date().getTime() + 7 * hour));
        if (type == true) {
            presensi1.setPrsensi(PresensiEnum.MASUK);

        }else {
            presensi1.setPrsensi(PresensiEnum.PULANG);
        }
        presensi1.setUsersId(usersRepository.findById(masukDto.getUserId()).orElseThrow(() -> new NotFoundException("Not Found")));
        return presensiRepository.save(presensi1);
    }

    @Override
    public Presensi absenPulang(MasukDto masukDto, boolean type) {
        Presensi presensi1 = new Presensi();
        presensi1.setMasuk(new Date(new Date().getTime() + 7 * hour));
        presensi1.setUsersId(usersRepository.findById(masukDto.getUserId()).orElseThrow(() -> new NotFoundException("Not Found")));
        presensi1.setPrsensi(PresensiEnum.PULANG);
        return presensiRepository.save(presensi1);
    }


//    @Override
//    public Presensi addPresensi(Presensi presensi) {
//        return presensiRepository.save(presensi);
//
//    }
//    @Override
//    public Presensi getPresensi(Long id) {
//        var presensi = presensiRepository.findById(id).get();
//        return presensiRepository.save(presensi);
//    }
//
//
//

    @Override
    public void deletePresensi(Long id) {
        presensiRepository.deleteById(id);
    }
}
