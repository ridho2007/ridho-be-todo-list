package com.example.PresensiToDoList.serviceImplements;

import com.example.PresensiToDoList.modal.UserPrinciple;
import com.example.PresensiToDoList.modal.Users;
import com.example.PresensiToDoList.repository.UsersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class UserDetailServiceImpl implements UserDetailsService {
    @Autowired
    private UsersRepository usersRepository;
    @Override
    public UserDetails loadUserByUsername(String username)throws UsernameNotFoundException{
        Users users = usersRepository.findByEmail(username).orElseThrow(()-> new UsernameNotFoundException("Usernam not found"));
        return UserPrinciple.build(users);

    }
}
