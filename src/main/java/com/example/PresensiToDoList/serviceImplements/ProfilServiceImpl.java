package com.example.PresensiToDoList.serviceImplements;

import com.example.PresensiToDoList.dto.ProfilDTO;
import com.example.PresensiToDoList.exception.NotFoundException;
import com.example.PresensiToDoList.modal.Profil;
import com.example.PresensiToDoList.repository.ProfilRepository;
import com.example.PresensiToDoList.repository.UsersRepository;
import com.example.PresensiToDoList.service.ProfilService;
import org.apache.commons.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Optional;

@Service
public class ProfilServiceImpl implements ProfilService {


    @Autowired
    ProfilRepository profilRepository;


    @Autowired
    UsersRepository usersRepository;

    @Override
    public Profil addProfil(ProfilDTO profil, MultipartFile multipartFile) {
        String file = convertToBase64Url(multipartFile);
        Profil profil1 = new Profil();
        profil1.setFoto(file);
        profil1.setNama(profil.getNama());
        profil1.setDeskripsi(profil.getDeskripsi());
        profil1.setAlamat(profil.getAlamat());
        profil1.setNomor(profil.getNomor());
        profil1.setUsersId(usersRepository.findById(profil.getUserId()).orElseThrow(() -> new NotFoundException("Not Found")));
        return profilRepository.save(profil1);
    }


    private String convertToBase64Url(MultipartFile file) {
        String url = "";
        try {
            byte[] byteData = Base64.encodeBase64(file.getBytes());
            String result = new String(byteData);
            url = "data:" + file.getContentType() + ";base64," + result;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            return url;
        }

    }

    @Override
    public Profil getProfil(Long userId) {
        return profilRepository.findById(userId).orElseThrow(() -> new NotFoundException("test"));
    }

    @Override
    public List<Profil> findUser(Long userId) {
        return profilRepository.finduser(userId);
    }


    @Override
    public Profil editProfil(Long id, ProfilDTO profil, MultipartFile multipartFile) {
        Profil profil1 = profilRepository.findById(id).get();
        String foto = convertToBase64Url(multipartFile);
        profil1.setNama(profil.getNama());
        profil1.setFoto(foto);
        profil1.setAlamat(profil.getAlamat());
        profil1.setNomor(profil.getNomor());
        profil1.setDeskripsi(profil.getDeskripsi());
        profil1.setUsersId(usersRepository.findById(profil.getUserId()).orElseThrow(() -> new NotFoundException("Not fOUND")));
        return profilRepository.save(profil1);
    }

    @Override
    public void deleteProfil(Long id) {
        profilRepository.deleteById(id);
    }
}
