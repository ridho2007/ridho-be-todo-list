package com.example.PresensiToDoList.jwt;

import com.example.PresensiToDoList.exception.InternalErrorException;
import com.example.PresensiToDoList.exception.NotFoundException;
import com.example.PresensiToDoList.modal.TemporaryToken;
import com.example.PresensiToDoList.modal.Users;
import com.example.PresensiToDoList.repository.TemporaryTokenRepository;
import com.example.PresensiToDoList.repository.UsersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.UUID;

@Component
public class JwtProvider {
    private static String seccetKey = "belajar springs";
    private static Integer expired = 900000;
    @Autowired
    private TemporaryTokenRepository temporaryTokenRepository;

    @Autowired
    private UsersRepository usersRepository;

    public String generateToken(UserDetails userDetails) {
        String token = UUID.randomUUID().toString().replace("-", "");
        Users user = usersRepository.findByEmail(userDetails.getUsername()).orElseThrow(() -> new NotFoundException("User not found generate token"));
        var checkingToken = temporaryTokenRepository.findByUserId(user.getId());
        if (checkingToken.isPresent()) temporaryTokenRepository.deleteById(checkingToken.get().getId());
        TemporaryToken temporaryToken = new TemporaryToken();
        temporaryToken.setToken(token);
        temporaryToken.setExpiredDate(new Date(new Date().getTime() + expired));
        temporaryToken.setUserId(user.getId());
        temporaryTokenRepository.save(temporaryToken);
        return token;

    }

    public TemporaryToken getSubject(String token) {
        return temporaryTokenRepository.findByToken(token).orElseThrow(() -> new InternalErrorException("Token Eror parse"));

    }

    public boolean checkingTokenJwt(String token) {
        TemporaryToken tokenExist = temporaryTokenRepository.findByToken(token).orElse(null);
        if (tokenExist == null) {
            System.out.println("token kosong");
            return false;
        }
        if (tokenExist.getExpiredDate().before(new Date())) {
            System.out.println("Token expired");
            return false;
        }
        return true;
    }
}
