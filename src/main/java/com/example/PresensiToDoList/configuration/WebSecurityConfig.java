package com.example.PresensiToDoList.configuration;

import com.example.PresensiToDoList.jwt.AccessDenied;
import com.example.PresensiToDoList.jwt.JwtAuthTokenFilter;
import com.example.PresensiToDoList.jwt.UnautorizeError;
import com.example.PresensiToDoList.serviceImplements.UserDetailServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
    @Autowired
    private AccessDenied accessDeniedHandler;
    @Autowired
    private UnautorizeError unautorizeError;
    @Autowired
    private UserDetailServiceImpl userDetailService;

    private static final String[] AUTH_WHITLIST = {"/v2/api-docs", "/swagger-resources", "/swagger-resources/**",
            // -- Swagger UI v3 (OpenAPI)
            "/v3/api-docs/**", "/swagger-ui/**","swagger-ui/**", "/authentication/**",
            "/user/**","/user/sign-in","/user/sign-up",
            "/presensi/{id}", "/presensi/**",
            "/api/image",
            "/profil","/profil/{id}","profil/user",
            "/absenmasuk/{id}","/absenmasuk/**",
            "/absenpulang/{id}","/absenpulang/**",
            "/","/todolist/all","/todolist/{id}","/todolist",};

    @Bean
    public JwtAuthTokenFilter authTokenFilter() {
        return new JwtAuthTokenFilter();
    }

    @Override
    public void configure(AuthenticationManagerBuilder authenticationManagerBuilder) throws Exception {
        authenticationManagerBuilder.userDetailsService(userDetailService).passwordEncoder(passwordEncoder());
    }

    @Override
    @Bean
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    protected void configure(HttpSecurity http) throws Exception {
        http.cors().and().csrf().disable().exceptionHandling().authenticationEntryPoint(unautorizeError).and().exceptionHandling().accessDeniedHandler(accessDeniedHandler).and().sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS).and().authorizeRequests().antMatchers(AUTH_WHITLIST).permitAll().anyRequest().authenticated();
        http.addFilterBefore(authTokenFilter(), UsernamePasswordAuthenticationFilter.class);
    }
}
