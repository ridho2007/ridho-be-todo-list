package com.example.PresensiToDoList.controller;

import com.example.PresensiToDoList.dto.MasukDto;
import com.example.PresensiToDoList.dto.PresensiDto;
import com.example.PresensiToDoList.dto.ProfilDTO;
import com.example.PresensiToDoList.modal.Presensi;
import com.example.PresensiToDoList.modal.Users;
import com.example.PresensiToDoList.response.CommonResponse;
import com.example.PresensiToDoList.response.ResponHelper;
import com.example.PresensiToDoList.service.PresensiService;
import com.example.PresensiToDoList.service.UsersService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequestMapping("/presensi")
@RestController
@CrossOrigin(origins = "http://localhost:3000")
public class PresensiController {

    @Autowired
    private PresensiService presensiService;

    @Autowired
    private ModelMapper modelMapper;
    @GetMapping
    public  CommonResponse<List<Presensi>> getAllPresensi(Long usersId){
        return ResponHelper.ok(presensiService.getAllPresensi(usersId));

    }
//    @GetMapping("/{id}")
//    public CommonResponse<Presensi> getPresensi(@PathVariable("id")Long id){
//        return ResponHelper.ok(presensiService.getPresensi(id));
//    }

    @PostMapping("absen_masuk")
    public CommonResponse<Presensi> absenMasuk( MasukDto masukDto) {
        return ResponHelper.ok(presensiService.absenMasuk(masukDto,true)) ;
    }
    @PostMapping("absen_pulang")
    public CommonResponse<Presensi> absenPulang( MasukDto masukDto) {
        return ResponHelper.ok(presensiService.absenPulang(masukDto,false)) ;
    }



    @DeleteMapping("/{id}")
    public void deletePresensi(@PathVariable("id") Long id) { presensiService.deletePresensi(id);}
}
