package com.example.PresensiToDoList.controller;


import com.example.PresensiToDoList.dto.*;
import com.example.PresensiToDoList.modal.Presensi;
import com.example.PresensiToDoList.modal.Profil;
import com.example.PresensiToDoList.modal.TodoList;
import com.example.PresensiToDoList.modal.Users;
import com.example.PresensiToDoList.response.CommonResponse;
import com.example.PresensiToDoList.response.ResponHelper;
import com.example.PresensiToDoList.service.ProfilService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Map;

@RequestMapping("/profil")
@RestController
public class ProfilController {

    @Autowired
    ProfilService profilService;

    @Autowired
    private ModelMapper modelMapper;
    
    @PostMapping(consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public CommonResponse<Profil>addProfil(ProfilDTO profilDTO , @RequestPart("foto") MultipartFile multipartFile ){
        return ResponHelper.ok(profilService.addProfil(profilDTO, multipartFile));
    }

    @GetMapping("/{id}")
    public CommonResponse<Profil> getProfil(@PathVariable("id") Long userId){
        return ResponHelper.ok(profilService.getProfil(userId));
    }

    @PutMapping(path = "/{id}",consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public CommonResponse<Profil> editProfilById(@PathVariable("id") Long id,  ProfilDTO profilDTO ,@RequestPart("file")MultipartFile multipartFile) {
        return ResponHelper.ok( profilService.editProfil( id,profilDTO ,multipartFile));
    }

    @DeleteMapping("/{id}")
    public void deleteProfil(@PathVariable("id") Long id) { profilService.deleteProfil(id);}

    @GetMapping("/user")
    public CommonResponse<List<Profil>> getUser(@RequestParam(name = "users_id") Long userId){
        return ResponHelper.ok(profilService.findUser(userId));
    }
    
}
