package com.example.PresensiToDoList.controller;

import com.example.PresensiToDoList.dto.PresensiDto;
import com.example.PresensiToDoList.dto.TodoListDto;
import com.example.PresensiToDoList.modal.Presensi;
import com.example.PresensiToDoList.modal.TodoList;
import com.example.PresensiToDoList.response.CommonResponse;
import com.example.PresensiToDoList.response.ResponHelper;
import com.example.PresensiToDoList.service.TodoListService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RequestMapping("/todolist")
@RestController
public class TodoListController {

    @Autowired
    TodoListService todoListService;

    @Autowired
    private ModelMapper modelMapper;
    @GetMapping
    public  Object getAllTodoList(Long usersId){
        return ResponHelper.ok(todoListService.getAllTodoList(usersId));

    }
    @DeleteMapping("/{id}")
    public void deleteTodoList(@PathVariable("id") Long id) { todoListService.deleteTodoList(id);}

    @PostMapping
    public CommonResponse<TodoList> addTodoList(TodoListDto todoListDto) {
        return ResponHelper.ok(todoListService.addTodoList(todoListDto)) ;
    }
    @GetMapping("/{id}")
    public CommonResponse<TodoList> getTodoList(@PathVariable("id")Long id){
        return ResponHelper.ok(todoListService.getTodoList(id));
    }

    @PutMapping(path = "/{id}")
    public CommonResponse<TodoList> editTodoListById(@PathVariable("id") Long id,@RequestBody TodoListDto todoListDto) {
        return ResponHelper.ok( todoListService.editTodoList(id, modelMapper.map(todoListDto, TodoList.class)));
    }

}
