package com.example.PresensiToDoList.modal;

import com.example.PresensiToDoList.enumated.PresensiEnum;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.hibernate.annotations.CreationTimestamp;


import javax.persistence.*;
import java.lang.String;
import java.util.Date;

@Entity
@Table(name ="presensi")
public class Presensi {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;


    @ManyToOne
    @JoinColumn(name = "users_id")
    private Users usersId;

    @JsonFormat(pattern = "yyyy-MM-dd ")
    @CreationTimestamp
    @Column(name = "tanggal")
    private Date tanggal;

    @JsonFormat(pattern = "HH-mm")

    @Column(name = "masuk")
    private Date masuk;

    public Date getMasuk() {
        return masuk;
    }

    public void setMasuk(Date masuk) {
        this.masuk = masuk;
    }

    @Enumerated(value = EnumType.STRING)
    @Column(name = "presensi")
    private PresensiEnum prsensi;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Users getUsersId() {
        return usersId;
    }

    public void setUsersId(Users usersId) {
        this.usersId = usersId;
    }

    public Date getTanggal() {
        return tanggal;
    }

    public void setTanggal(Date tanggal) {
        this.tanggal = tanggal;
    }

    public PresensiEnum getPrsensi() {
        return prsensi;
    }

    public void setPrsensi(PresensiEnum prsensi) {
        this.prsensi = prsensi;
    }
}
