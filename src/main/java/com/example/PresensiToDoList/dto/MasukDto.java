package com.example.PresensiToDoList.dto;

import com.example.PresensiToDoList.enumated.PresensiEnum;

public class MasukDto {
    private Long userId;
private PresensiEnum presensi;

    public PresensiEnum getPresensi() {
        return presensi;
    }

    public void setPresensi(PresensiEnum presensi) {
        this.presensi = presensi;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }
}
