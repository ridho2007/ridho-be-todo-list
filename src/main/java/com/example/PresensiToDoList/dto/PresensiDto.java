package com.example.PresensiToDoList.dto;

import java.lang.String;
import java.util.Date;

public class PresensiDto {

    private String absenMasuk;
    private String absenKeluar;



    private Long userId;

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getAbsenMasuk() {
        return absenMasuk;
    }

    public void setAbsenMasuk(String absenMasuk) {
        this.absenMasuk = absenMasuk;
    }

    public String getAbsenKeluar() {
        return absenKeluar;
    }

    public void setAbsenKeluar(String absenKeluar) {
        this.absenKeluar = absenKeluar;
    }


}
