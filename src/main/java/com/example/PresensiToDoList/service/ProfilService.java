package com.example.PresensiToDoList.service;

import com.example.PresensiToDoList.dto.ProfilDTO;
import com.example.PresensiToDoList.modal.Profil;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Optional;

public interface ProfilService {

    Profil getProfil(Long userId);

    List<Profil> findUser(Long id);

    Profil addProfil(ProfilDTO profil, MultipartFile multipartFile);

    Profil editProfil(Long id, ProfilDTO profil, MultipartFile multipartFile);

    void deleteProfil(Long id);
}
