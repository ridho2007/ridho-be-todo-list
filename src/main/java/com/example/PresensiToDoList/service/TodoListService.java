package com.example.PresensiToDoList.service;

import com.example.PresensiToDoList.dto.TodoListDto;
import com.example.PresensiToDoList.modal.TodoList;

public interface TodoListService {

    Object getAllTodoList(Long usersId);

    TodoList getTodoList(Long id);
    TodoList addTodoList(TodoListDto presensiDto);

    TodoList editTodoList(Long id,TodoList todoList);

    void deleteTodoList(Long id);
}
