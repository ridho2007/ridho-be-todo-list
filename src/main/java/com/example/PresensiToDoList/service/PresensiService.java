package com.example.PresensiToDoList.service;

import com.example.PresensiToDoList.dto.MasukDto;
import com.example.PresensiToDoList.modal.Presensi;

import java.util.List;


public interface PresensiService {
  List<Presensi> getAllPresensi(Long usersId);
    Presensi absenMasuk(MasukDto masukDto,boolean type);

    Presensi absenPulang(MasukDto masukDto,boolean type);

//    Presensi getPresensi(Long id);

void deletePresensi(Long id);
}
